<?php

namespace controllers;

use pages\Overview;
use repositories\QueryRepository;

class OverviewController
{

    public function selectPageToRender() {

        $page = isset($_GET["page"]) ? $_GET["page"] : null;

        switch ($page) {
            case "Overview":
            return $this->showOverview();
            break;
            default:
            return $this->showOverview();
            break;
        }
    }
    protected function showOverview() {
        $page = new Overview();
        return  $page->Render();
    }
}

