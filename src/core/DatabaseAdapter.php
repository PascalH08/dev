<?php

namespace core;

use DatabaseAdapter\config;
use mysqli;

class DatabaseAdapter
{

    /** @var DatabaseAdapter|null */
    private static $_instance = null;
    /** @var mysqli|null */
    private $_connection = null;

    /**
     * TODO desc
     *
     * DatabaseAdapter constructor.
     * @param $host
     * @param $port
     * @param $user
     * @param $pass
     * @param $database
     */
    private function __construct($host, $port, $user, $pass, $database)
    {
        $this->_connection = new mysqli ($host, $user, $pass, $database, $port);

        if ($this->_connection->connect_errno) {
            die("Failed to connect to Database");
        }
    }

    /**
     * TODO desc
     *
     * @return DatabaseAdapter|null
     */
    public static function getInstance()
    {

        if (self::$_instance === null) {
            self::$_instance = new DatabaseAdapter(Config::$DB_Host, Config::$DB_Port, Config::$DB_User, Config::$DB_Password, Config::$DB_Database);
        }

        return self::$_instance;
    }

    /**
     * TODO desc
     *
     * @param $query
     * @param array $properties
     * @return mixed
     */
    public function query($query, $properties = [])
    {

        if (!($stmt = $this->_connection->prepare($query))) {
            echo "Prepare failed";
        }

        $stmt->execute();
        $result = $stmt->get_result();

        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            null;
        }
    }

    /**
     * TODO desc
     *
     * @param $query
     * @return bool
     */
    public function execute($query)
    {

        if (!($stmt = $this->_connection->prepare($query))) {
            echo "Prepare failed";
        }

        return $stmt->execute();
    }

    public function real_escape_string($string)
    {
        return $this->_connection->real_escape_string($string);
    }

    public function getLastInsertID() {
        return intval($this->_connection->insert_id);
    }

}
