<?php


namespace core;


abstract class AbstractTemplate
{

    public abstract function FillRender();

    protected function StartRender() {
        ob_start();
    }

    protected function EndRender() {
        return ob_get_clean();
    }

    public function Render() {
        $this->StartRender();
        $this->FillRender();
        return $this->EndRender();
    }

    public function Print() {
        echo $this->Render();
    }

}