<?php

namespace repositories;

use core\DatabaseAdapter;
use Exception;

class QueryRepository
{

    /** @var QueryRepository */
    private $_provider;

    /**
     * QuestionnaireRepository constructor.
     * @param $provider
     */
    private function __construct($provider)
    {
        $this->_provider = $provider;
    }

    /**
     * TODO desc
     *
     * @return QueryRepository
     */
    public static function getDefault()
    {
        return new QueryRepository(DatabaseAdapter::getInstance());
    }

}