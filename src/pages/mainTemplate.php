<?php

namespace pages;

use core\AbstractTemplate;

session_start();


class mainTemplate extends AbstractTemplate
{

    public $head = "";
    public $content = "";
    public $footer = "";


    public function FillRender()
    {
        ?>

        <!DOCTYPE html>
        <html>
        <head>

            <!-- JS -->
            <script src="./js/jquery-3.5.1.min.js"></script>
            <script src="./js/bootstrap.min.js"></script>
            <!-- CSS -->
            <link rel="stylesheet" href="./css/header.css">
            <link rel="stylesheet" href="./css/footer.css">
            <link rel="stylesheet" href="./css/bootstrap.min.css">
            <link rel="stylesheet" href="./css/mainTemplate.css">
        


            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Dive</title>
        </head>

        <body>
        <div id="wrapper">
            <div class="head_height"><?= $this->head; ?></div>
            <div class="content_height"><?= $this->content; ?></div>
            <div class="footer_height"><?= $this->footer; ?></div>
        </div>
        </body>

        </html>

        <?php
    }
}