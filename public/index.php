<?php

use pages\Footer;
use pages\mainTemplate;
use pages\head;
use controllers\OverviewController;
use pages\ProductionStructureNew;

require_once "../src/repositories/QueryRepository.php";
require_once "../src/core/DatabaseAdapter.php";
require_once "../src/core/AbstractTemplate.php";
require_once "../config/config.php";
require_once "../src/pages/mainTemplate.php";
require_once "../src/pages/head.php";
require_once "../src/pages/Footer.php";
require_once "../src/controllers/OverviewController.php";
require_once "../src/pages/Overview.php";



$mainTmpl = new mainTemplate();

$mainTmpl->head = (new head())->Render();
$mainTmpl->content = (new OverviewController())->selectPageToRender();
$mainTmpl->footer = (new Footer())->Render();

$mainTmpl->Print();